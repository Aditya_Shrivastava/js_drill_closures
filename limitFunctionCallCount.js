
function limitFunctionCallCount(cb, n) {
    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned
    if(typeof n !== 'number'){
      console.log('n should be a number')
      return
    }
    let counter = 1
    return function(){
      if(counter <= n){
        cb()
        counter++
      }
      else{
        console.log('not allowed')
      }
    }
}
module.exports = limitFunctionCallCount
