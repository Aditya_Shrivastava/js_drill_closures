function counterFactory() {
    let value = 0;
    function changeValueBy(val){
      value += val;
    }
    return{
      increment(){
        changeValueBy(1)
        return value
      },
      decrement(){
        changeValueBy(-1)
        return value
      },
      printValue(){
        console.log(value)
      }
    }
}
module.exports = counterFactory
